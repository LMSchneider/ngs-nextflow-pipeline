nextflow.enable.dsl = 2
  
params.with_stats = false
params.with_fastqc = false
params.with_fastp = false

//  container "https://depot.galaxyproject.org/singularity/"

process prefetch {
  storeDir "${params.outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra"
  script:
    """
    prefetch $accession
    """
}

process showFile {
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  input:
    path sraresult
  output:
    path "fileinfo.txt"
  script:
    """
    echo "${sraresult}" > fileinfo.txt
    """
}

// fastq_dump
process fastq_dump {
  publishDir "${params.outdir}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input:
    path infile
  output:
    path "*.fastq"
  script:
    """
    fastq-dump --split-e ${infile} > "${infile}.splitFilesSRA"
    """
}

process stats {
  publishDir "${params.outdir}/stats", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/ngsutils%3A0.5.9--py27heb79e2c_4"
  input:
    path infile
  output:
    path "${infile.getSimpleName()}.txt"
  script:
    """
     fastqutils stats ${infile} > ${infile.getSimpleName()}.txt
    """
}

//FASTQC einbinden
process fastqc{
  publishDir "${params.outdir}", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
  input:
    path infile
  output:
    path "fastqc_results/*" // oder ${infile.getSimpleName()}*fastq*
  script:
  """
  mkdir fastqc_results
  fastqc -o fastqc_results ${infile} 
  """
}

// FASTP einbinden - trimming

process fastp {
  publishDir "${params.outdir}", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0"
  input:
    path infile
  output:
    path "trimmed_${infile.getSimpleName()}.fastq", emit: fastqtrim
    path "${infile.getSimpleName()}_trimmed_fastp.json", emit: jsontrim
  script:
  """
  fastp -i ${infile} -o trimmed_${infile.getSimpleName()}.fastq -j ${infile.getSimpleName()}_trimmed_fastp.json
  """

}

// MultiQC einbinden, single report for many samples
process multiqc {
  publishDir "${params.outdir}/multiqc", mode: "copy" , overwrite: true
  container "https://depot.galaxyproject.org/singularity/multiqc%3A1.9--pyh9f0ad1d_0"
  input:
    path infile
  output:
    path "*"
  script:
  """
    multiqc ${infile}
  """
}

workflow vorbereitung {
  take:
    accession
    outdir
    with_stats
    with_fastqc
    with_fastp
  main:
    params.vorbereitung_outdir = outdir
    sraresult = prefetch(accession)
    fastas = fastq_dump(sraresult)
    if (with_stats) {
      stats(fastas.flatten())
    }
    fastas_trimmed_fastq = channel.empty()
    if (with_fastp) {
      fastas_trimmed = fastp(fastas.flatten())
      fastas_trimmed_fastq = fastas_trimmed.fastqtrim
      fastas_trimmed_json = fastas_trimmed.jsontrim
    }
    if (params.with_fastqc) {
        fastqc_results = fastqc(fastas_trimmed_fastq)
    }
  emit:
    results_trimmed = fastas_trimmed_fastq.collect()
    fastqc_results
    fastas_trimmed_json
}



workflow {
// SRA File Downloaden
  sraresult = prefetch(params.accession)
// FILE anzeigen
  showfile = showFile(sraresult)
// in FASTA Dateien splitten
  fastas = fastq_dump(sraresult)
// Generelle Statistik
  if (params.with_stats) {
    stats(fastas.flatten())
  }
// Trimming - fakultativ
  fastas_trimmed_fastq = channel.empty()
  
  if (params.with_fastp) {
    fastas_trimmed = fastp(fastas.flatten())
    fastas_trimmed_fastq = fastas_trimmed.fastqtrim
    fastas_trimmed_json = fastas_trimmed.jsontrim
  }
// fastas und fastas_trimmed konkatenieren für fastqc input
all_fasta_channel = fastas.flatten().concat(fastas_trimmed_fastq.flatten())
// Quality control

  if (params.with_fastqc) {
    all_fastqc = fastqc(all_fasta_channel)
  }
// multiQC
  multiqc(all_fastqc.collect().concat(fastas_trimmed_json).collect())
}
